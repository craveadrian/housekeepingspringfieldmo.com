<div id="content">
	<div id="details-section">
		<div class="row">
			<div class="dsLeft inbTop">
				<h1>WE CARE ABOUT<span>THE DETAILS</span></h1>
			</div>
			<div class="dsRight inbTop">
				<p>Our company pays special attention to the details of every project to ensure the complete satisfaction of each client. ​Our goal is to make sure that we leave your home with sparkling kitchen, bathrooms, and floors, organized and tidy living spaces, and refreshing aromas of cleanliness.</p>
			</div>
		</div>
	</div>
	<div id="satisfaction-section">
		<div class="row">
			<div class="ssText">
				<h2>CUSTOMER SATISFACTION<span>GUARANTEED</span></h2>
				<p>Your satisfaction is our priority and we strive to provide a service we are proud of. We are always prepared for any cleaning requirements and will complete your requested tasks in a timely manner with detail and precision, and at a price you’ll love. </p>
				<a href="<?php echo URL; ?>about#content" class="btn">LEARN MORE</a>
				<p class="phone">CALL US TODAY!<span><?php $this->info(["phone","telto"]); ?></span> </p>
			</div>
			<img src="public/images/content/img1.jpg" alt="" class="img1">
			<img src="public/images/content/img1right.jpg" alt="" class="img1Right">
			<p class="socialMedia">
				<a href="<?php $this->info("fb_link"); ?>" class="socialico" >f</a>
				<a href="<?php $this->info("tt_link"); ?>" class="socialico" >l</a>
				<a href="<?php $this->info("yt_link"); ?>" class="socialico" >x</a>
				<a href="<?php $this->info("rss_link"); ?>" class="socialico" >r</a>
			</p>
		</div>
	</div>
	<div id="service-section">
		<div class="row">
			<h1>WHAT WE DO</h1>
			<dl class="inbTop">
				<dt> <img src="public/images/content/service1.jpg" alt="General Cleaning"> </dt>
				<dd>GENERAL CLEANING</dd>
			</dl>
			<dl class="mid inbTop">
				<dt> <img src="public/images/content/service2.jpg" alt="Deep Cleaning"> </dt>
				<dd>DEEP CLEANING</dd>
			</dl>
			<dl class="inbTop">
				<dt> <img src="public/images/content/service3.jpg" alt="Window Cleaning"> </dt>
				<dd>WINDOW CLEANING</dd>
			</dl>
		</div>
	</div>
	<div id="reviews-section">
		<div class="row">
			<img src="public/images/content/img2.jpg" alt="" class="img2">
			<img src="public/images/content/img1right.jpg" alt="" class="img1Right">
			<p class="socialMedia">
				<a href="<?php $this->info("fb_link"); ?>" class="socialico" >f</a>
				<a href="<?php $this->info("tt_link"); ?>" class="socialico" >l</a>
				<a href="<?php $this->info("yt_link"); ?>" class="socialico" >x</a>
				<a href="<?php $this->info("rss_link"); ?>" class="socialico" >r</a>
			</p>
			<div class="rsText fr">
				<h1>WHAT THEY SAY</h1>
				<p>“Professional, punctual, and thorough: we can’t recommend them enough!” </p>
				<p class="author">- RODGER SODINI</p>
				<p>“My house is sparkling clean after every visit; they care about the details and take pride in th eir work.” </p>
				<p class="author">- KATHY S.</p>
				<a href="<?php echo URL ?>about#content" class="btn">READ MORE</a>
			</div>
			<div class="clearfix">
			</div>
		</div>
	</div>
	<div id="gallery-section">
		<div class="row">
			<h1>OUR GALLERY</h1>
			<div class="gLeft inbTop imgGallery">
				<img src="public/images/content/gallery1.jpg" alt="Gallery 1">
				<img src="public/images/content/gallery2.jpg" alt="Gallery 1">
			</div>
			<div class="gMid inbTop imgGallery">
				<img src="public/images/content/gallery3.jpg" alt="Gallery 1">
				<img src="public/images/content/gallery4.jpg" alt="Gallery 1">
			</div>
			<div class="gRight inbTop imgGallery">
				<img src="public/images/content/gallery5.jpg" alt="Gallery 1">
				<img src="public/images/content/gallery6.jpg" alt="Gallery 1">
			</div>
			<a href="<?php echo URL; ?>gallery#content" class="btn">VIEW MORE</a>
		</div>
	</div>
	<div id="qform-section">
		<div class="row">
			<h1>KEEP IN TOUCH</h1>
			<form action="sendContactForm" method="post"  class="sends-email ctc-form" >
				<div class="col fl">
					<input type="text" name="name" placeholder="Name">
					<input type="text" name="email" placeholder="Email">
				</div>
				<div class="col fr">
					<input type="text" name="confirm_email" placeholder="Confirm Email">
					<input type="text" name="phone" placeholder="Phone">
				</div>
				<textarea name="message" cols="30" rows="10" placeholder="Message:"></textarea>
				<div class="g-recaptcha"></div>
				<p>
					<input type="checkbox" name="consent" class="consentBox">I hereby consent to having this website store my submitted information so that they can respond to my inquiry.
				</p>
				<?php if( $this->siteInfo['policy_link'] ): ?>
				<p>
					<input type="checkbox" name="termsConditions" class="termsBox"/> I hereby confirm that I have read and understood this website's <a href="<?php $this->info("policy_link"); ?>" target="_blank">Privacy Policy.</a>
				</p>
				<?php endif ?>
				<button type="submit" class="ctcBtn btn" disabled>SUBMIT FORM</button>
			</form>
		</div>
		<div class="clearfix"></div>
	</div>
	<div id="contact-details-section">
		<div class="row">
			<div class="cdsLeft fl">
				<img src="public/images/common/mainLogo.png" alt="Clair's Cleaning Fairies Logo">
				<p>We will be glad to answer your questions, feel free to ask a piece of information or a quotation. We are looking forward to work with you.</p>
			</div>
			<div class="cdsMid fl">
				<div class="info">
					<img src="public/images/common/phone.png" alt="Phone Icon">
					<p>PHONE <span class="phone"><?php $this->info(["phone","tel"]); ?></span> </p>
				</div>
				<div class="info">
					<img src="public/images/common/email.png" alt="Email Icon">
					<p>EMAIL <span><?php $this->info(["email","mailto"]); ?></span> </p>
				</div>
				<div class="info">
					<img src="public/images/common/location.png" alt="Location Icon">
					<p>SERVICE AREAS <span><?php $this->info("address"); ?></span></p>
				</div>
			</div>
			<div class="cdsRight fr">
				<div class="maps"><iframe width="100%" height="317" src="https://maps.google.com/maps?width=720&amp;height=600&amp;hl=en&amp;q=Springfield%2C%20MO+(CLAIR%E2%80%99S%20CLEANING%20FAIRIES)&amp;ie=UTF8&amp;t=&amp;z=14&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"><a href="https://www.maps.ie/create-google-map/">Add map to website</a></iframe></div><br />
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
